import os
import http_api

APP_NAME = os.getenv('app') or 'GPT.git'

http_api.main(app_name=APP_NAME)
