import typing
import json
import logging
import logging.config

import gitlab_client
import repo_url_parser
import aiohttp
from aiohttp import web
from aiohttp import web_request
import aiohttp_cors

from logger import setup_logging

BASIC_PARAMS = typing.Tuple[logging.Logger, repo_url_parser.RepoUrl, gitlab_client.GitlabClient]

EXCEPTION_TO_RESPONSE = {
    gitlab_client.InsufficientPrivileges: (401, 'check your private token'),
    gitlab_client.RepositoryNotFound: (404, 'repository not found'),
    gitlab_client.InternalError: (500, 'internal server error'),
    Exception: (500, 'unexpected behavior'),
}

FILE_LOGO_PATH = 'assets/logo.png'
FILE_AI_PLUGIN_PATH = 'ai-plugin.json'
FILE_OPENAPI_PATH = 'openapi.yaml'


def _handle_exceptions(exception: Exception):
    status, reason = EXCEPTION_TO_RESPONSE.get(type(exception), (500, 'unhandled exception'))
    return web.Response(status=status, reason=reason)


def _get_basic_parameters(request: web.Request) -> BASIC_PARAMS:
    logger = request.app.logger
    encoded_repo_url = request.match_info.get('encoded_repository_url', None)
    private_token = request.query.get('private_token', None)
    repo_url = repo_url_parser.RepoUrl(encoded_repo_url)
    gitlab = gitlab_client.GitlabClient(host=repo_url.host, token=private_token, logger=logger)
    return logger, repo_url, gitlab


async def get_project_tree(request: aiohttp.web_request.Request) -> web.Response:
    logger, repo_url, gitlab = _get_basic_parameters(request)
    try:
        project_tree = await gitlab.get_project_tree(repo_url.urlencoded_project_path)
        return web.Response(text=json.dumps(project_tree), status=200)
    except Exception as e:
        logger.error(e)
        return _handle_exceptions(e)


async def get_blob_content(request: aiohttp.web_request.Request) -> web.Response:
    logger, repo_url, gitlab = _get_basic_parameters(request)
    blob_sha = request.match_info.get('file_id', None)
    try:
        blob_content = await gitlab.get_blob_content(repo_url.urlencoded_project_path, blob_sha=blob_sha)
        return web.Response(text=blob_content, status=200)
    except Exception as e:
        logger.error(e)
        return _handle_exceptions(e)


async def get_logo(_: aiohttp.web_request.Request) -> web.FileResponse:
    return web.FileResponse(FILE_LOGO_PATH)


async def get_ai_plugin_json(_: aiohttp.web_request.Request) -> web.FileResponse:
    return web.FileResponse(FILE_AI_PLUGIN_PATH)


async def get_openapi_yaml(_: aiohttp.web_request.Request) -> web.FileResponse:
    return web.FileResponse(FILE_OPENAPI_PATH)


def main(app_name: str) -> typing.NoReturn:
    setup_logging(app_name)

    main_logger = logging.getLogger(app_name)
    app = web.Application()
    aiohttp_cors.setup(app)

    app.add_routes(
        [
            web.get('/repository/{encoded_repository_url}/tree', get_project_tree),
            web.get('/repository/{encoded_repository_url}/files/{file_id}/raw', get_blob_content),
            web.get('/logo.png', get_logo),
            web.get('/.well-known/ai-plugin.json', get_ai_plugin_json),
            web.get('/openapi.yaml', get_openapi_yaml),
        ]
    )

    cors = aiohttp_cors.setup(app, defaults={
        "https://chat.openai.com": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*"
        )
    })

    for route in list(app.router.routes()):
        cors.add(route)

    app.logger = logging.getLogger(app_name)
    web.run_app(app, host='0.0.0.0', port=5003, access_log=main_logger)
