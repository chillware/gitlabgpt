import urllib.parse


class RepoUrl:
    def __init__(self, encoded_url: str):
        self._url = urllib.parse.unquote(encoded_url)
        self._parsed_url = urllib.parse.urlparse(self._url)

    @property
    def host(self) -> str:
        scheme = self._parsed_url.scheme
        netloc = self._parsed_url.netloc
        return f"{scheme}://{netloc}"

    @property
    def urlencoded_project_path(self) -> str:
        return urllib.parse.quote(self._parsed_url.path.lstrip('/'), safe="")
