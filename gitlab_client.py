import logging
from functools import wraps
from typing import List, Dict, Any, Callable

import aiohttp
from aiohttp import TraceRequestStartParams, TraceRequestExceptionParams, TraceRequestEndParams


class RepositoryNotFound(Exception):
    pass


class InsufficientPrivileges(Exception):
    pass


class InternalError(Exception):
    pass


async def _handle_errors(response: aiohttp.ClientResponse):
    if response.status == 404:
        raise RepositoryNotFound("repository not found: %s", await response.text())
    if response.status == 403:
        raise InsufficientPrivileges(await response.text())
    if not response.ok:
        raise InternalError()


class GitlabClient:
    get_repository_tree_url = '{host}/api/v4/projects/{id}/repository/tree'
    get_blob_content_url = '{host}/api/v4/projects/{id}/repository/blobs/{sha}/raw'
    get_projects_list_url = '{host}/api/v4/projects?min_access_level=10'

    def __init__(self, host: str, token: str = None, logger: logging.Logger = None):
        self.host = host
        self.token = token
        self.logger = logger or logging.root
        self._trace_config = aiohttp.TraceConfig()
        self._trace_config.on_request_start.append(self._on_req_start)
        self._trace_config.on_request_end.append(self._on_req_end)
        self._trace_config.on_request_exception.append(self._on_req_end)

    async def _on_req_start(self, _, __, params: TraceRequestStartParams):
        self.logger.info('Starting request: [%s] %s', params.method, params.url)

    async def _on_req_failed(self, _, __, params: TraceRequestExceptionParams):
        self.logger.info('Failed request: [%s] %s: %e', params.method, params.url, params.exception)

    async def _on_req_end(self, _, __, params: TraceRequestEndParams):
        if 'response' in params.__slots__:
            self.logger.info('Completed request: [%s]%s -> %d', params.method, params.url, params.response.status)
        else:
            self.logger.info('Completed request: [%s]%s', params.method, params.url)

    def _with_private_token(self, url: str) -> str:
        if '?' in url:
            return f'{url}&private_token={self.token}'
        return f'{url}?private_token={self.token}'

    def _format_url(self, template: str, **values: str):
        result = template.format(**values)
        if self.token is None:
            return result
        return self._with_private_token(result)

    async def get_project_tree(self, project_id: str) -> List[Dict[str, Any]]:
        self.logger.info('init connection to host: %s', self.host)
        async with aiohttp.ClientSession(trace_configs=[self._trace_config]) as session:
            url = self._format_url(self.get_repository_tree_url, host=self.host, id=project_id)
            async with session.get(url) as response:
                await _handle_errors(response)
                return await response.json()

    async def get_blob_content(self, project_id: str, blob_sha: str) -> str:
        self.logger.info('init connection to host: %s', self.host)
        async with aiohttp.ClientSession(trace_configs=[self._trace_config]) as session:
            url = self._format_url(self.get_blob_content_url, host=self.host, id=project_id, sha=blob_sha)
            async with session.get(url) as response:
                await _handle_errors(response)
                return await response.text()
