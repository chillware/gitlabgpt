import logging
import logging.config


class ExcludeErrorsFilter(logging.Filter):
    def filter(self, record):
        return record.levelno < logging.ERROR


def setup_logging(project_name: str) -> None:
    logging_config = {
        'version': 1,
        'formatters': {
            'simple': {
                'format': '%(levelname)s: %(message)s'
            }
        },
        'filters': {
            'exclude_errors': {
                '()': ExcludeErrorsFilter
            }
        },
        'handlers': {
            'console_info': {
                'class': 'logging.StreamHandler',
                'level': 'DEBUG',
                'formatter': 'simple',
                'filters': ['exclude_errors'],
                'stream': 'ext://sys.stdout'
            },
            'console_error': {
                'class': 'logging.StreamHandler',
                'level': 'ERROR',
                'formatter': 'simple',
                'stream': 'ext://sys.stderr'
            }
        },
        'loggers': {
            project_name: {
                'handlers': ['console_info', 'console_error'],
                'level': 'DEBUG',
            }
        }
    }

    logging.config.dictConfig(logging_config)
